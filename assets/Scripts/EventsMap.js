const eventsMap = {
    'GUEST_HAS_WALKED_TO_RESTAURANT' : 'GuestHasWalkedToRestaurant',
    'GUEST_ARE_WAITING_FOR_TABLE' : 'GuestAreWaitingForTable',
    'MEET_A_GUEST' : 'MeetAGuest',
    'MEET_A_GUEST_STARTED' : 'MeetAGuestStarted',
    'MEET_A_GUEST_FINISHED' : 'MeetAGuestFinished',
    'TABLE_WAS_CHECKED' : 'TableWasChecked',
    'GUEST_ARE_CREATING_ORDER' : 'GuestAreCreatingOrder',
    'ORDER_WAS_CREATED' : 'OrderWasCreated',
    'GUEST_ARE_READY_FOR_ORDER' : 'GuestAreReadyForOrder',
    'WAITER_GO_TO_GUEST_FOR_ORDER' : 'WaiterGoToGuestForOrder',
    'ORDER_WAS_ACCEPTED' : 'OrderWasAccepted',
    'ORDER_WAS_BROUGHT_ON_KITCHEN' : 'OrderWasBroughtOnKitchen',
    'CHEF_TOOK_ORDER' : 'ChefTookOrder',
    'CALL_WAITER_FOR_TAKE_ORDER' : 'CallWaiterForTakeOrder',
    'WAITER_ARE_TAKING_ORDER_FROM_KITCHEN' : 'WaiterAreTakingOrderFromKitchen',
    'WAITER_TOOK_ORDER' : 'WaiterTookOrder',
    'SHOW_WAITER_TRAY' : 'ShowWaiterTray',
    'CLOSE_WAITER_TRAY' : 'CloseWaiterTray',
    'FREEZE_BACKGROUND' : 'FreezeBackground',
    'UNFREEZE_BACKGROUND' : 'UnfreezeBackground',
    'SHOW_DIALOG' : 'ShowDialog',
    'CHECKED_ORDER_ON_TRAY' : 'CheckOrderOnTray'
};

exports.getEventName = function (key) {
    return (typeof eventsMap[key] != 'undefined') ? eventsMap[key] : 'undefined';
};

exports.getClickEvent = function () {

}
