const EventsMap = require('EventsMap');

const WAITING_FOR_WAITER = 1;
const WALK_TO_CHECKED_TABLE = 2;
const CREATING_ORDER = 3;
const READY_FOR_ORDER = 4;
const WAITING_FOR_ORDER = 5;

const PB_DISABLED = 1;
const PB_CREATING_ORDER = 2;
cc.Class({
    extends: cc.Component,

    properties: {
        number: {default: null},
        state: {default: WAITING_FOR_WAITER},
        waitingForTableButton: { default: null, type: cc.Button},
        readyForOrderButton: { default: null, type: cc.Button},
        itIsMyOrderButton: { default: null, type: cc.Button},
        BillButton: { default: null, type: cc.Button},
        progressBar: {default: null, type: cc.ProgressBar},

        //progress bar
        progressBarState: {default : PB_DISABLED},
        creatingOrderTimeAmount: {default: 0},
        creatingOrderTime: {default: null},

        checkedTable: { default: null},
        currentOrder: { default: null},
        timeToOrderEnd: { default: null}
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function() {
        cc.log('Guest loaded');

        cc.systemEvent.on(EventsMap.getEventName('TABLE_WAS_CHECKED'), this.onTableWasChecked, this);
        cc.systemEvent.on(EventsMap.getEventName('MEET_A_GUEST_STARTED'), this.onMeetAGuestStarted, this);
        cc.systemEvent.on(EventsMap.getEventName('WAITER_GO_TO_GUEST_FOR_ORDER'), this.onWaiterGoToTableForOrder, this);
        cc.systemEvent.on(EventsMap.getEventName('ORDER_WAS_ACCEPTED'), this.onOrderWasAccepted, this);

        //button events
        this.waitingForTableButton.node.on('click', this.onWaitingForTableButtonClick, this);
        this.readyForOrderButton.node.on('click', this.onReadyForOrderButtonClick, this);
    },

    start: function() {},

    setNumber: function(number) {
        this.number = number;
    },
    
    walkToRestaurant: function (position) {
        var position = position;
        var walkToRestaurant = this.node.getComponent(cc.Animation).play('walkToRestaurant' + position);
        walkToRestaurant.on("finished", this.onWalkToRestaurantFinished, this);
    },

    onWalkToRestaurantFinished: function () {
        var eventName = EventsMap.getEventName('GUEST_HAS_WALKED_TO_RESTAURANT');
        cc.log(eventName + ' has started.');
        this.waitingForTableButton.node.opacity = 255;
        this.waitingForTableButton.node.zIndex = 999;
    },

    onMeetAGuestStarted: function() {
        if (this.state == WAITING_FOR_WAITER) {
            this.waitingForTableButton.node.opacity = 0;
            this.waitingForTableButton.node.zIndex = 0;
            this.waitingForTableButton.node.off('click', this.onWaitingForTableButtonClick, this);
        }
    },

    onWaitingForTableButtonClick: function (event) {
        cc.log('Guest : waiting for table');
        var eventName = EventsMap.getEventName('GUEST_ARE_WAITING_FOR_TABLE');
        var event = new Event(eventName);
        event.data = {"position" : this.number};
        cc.systemEvent.dispatchEvent(event);
    },

    onTableWasChecked: function (event) {
        this.state = WALK_TO_CHECKED_TABLE;
        cc.log("Guest : Table was checked : table#" + event.data.tableNumber);
        this.checkedTable = event.data.tableNumber;
        var walkToTable = this.node.getComponent(cc.Animation).play('walkToTable' + event.data.tableNumber + 'FromDoor');
        walkToTable.on("finished", this.walkToTableFinished, this);
    },

    walkToTableFinished: function () {
        this.state = CREATING_ORDER;
        this.creatingOrderTime = 2;
        this.progressBar.node.opacity = 255;
        this.progressBarState = PB_CREATING_ORDER;
        var eventName = EventsMap.getEventName('GUEST_ARE_CREATING_ORDER');
        var event = new Event(eventName);
        cc.systemEvent.dispatchEvent(event);
    },

    orderWasCreated: function() {
        this.state = READY_FOR_ORDER;
        this.creatingOrderTime = null;
        this.progressBar.node.opacity = 0;
        this.progressBar.node.zIndex = 0;
        this.progressBarState = PB_DISABLED;
        this.readyForOrderButton.node.opacity = 255;
        this.readyForOrderButton.node.zIndex = 999;
        this.currentOrder = {
            "id" : Math.round(Math.random()*10000),
            "items" : [
                {"name" : "coffee", "count" : 1, "type" : 1, "time" : 5, "cost" : 15, "status" : 1, "tableNumber" : this.checkedTable},
                {"name" : "bread", "count" : 2, "type" : 2, "time" : 3, "cost" : 8, "status" : 1, "tableNumber" : this.checkedTable}]
        };
        this.timeToOrderEnd = 60;
    },

    onReadyForOrderButtonClick: function() {
        cc.log('Ready for order. Guest number ' + this.number);
        var eventName = EventsMap.getEventName('GUEST_ARE_READY_FOR_ORDER');
        var event = new Event(eventName);
        event.data = {"tableNumber" : this.checkedTable, "order" : this.currentOrder};
        cc.systemEvent.dispatchEvent(event);
    },

    onWaiterGoToTableForOrder: function(event) {
        if ((this.state == READY_FOR_ORDER) && (this.checkedTable == event.data.tableNumber)) {
            cc.log('Guest : waiter go to table');
            this.readyForOrderButton.node.opacity = 0;
            this.readyForOrderButton.node.zIndex = 0;
            this.readyForOrderButton.node.off('click', this.onReadyForOrderButtonClick, this);
        }
    },

    onOrderWasAccepted: function(event) {
        cc.log('GUEST : Order was accepted by waiter.');
        this.state = WAITING_FOR_ORDER;
        this.progressBarState = PB_CREATING_ORDER;
        this.progressBar.node.opacity = 255;
    },

    goOutFromRestaurant: function() {
        this.progressBar.node.opacity = 0;
        cc.log('GO OUT FROM THIS RESTAURANT!!!');
    },

    update: function(dt) {
        if (this.progressBarState == PB_CREATING_ORDER){
            if (this.creatingOrderTime !== null) {
                this.creatingOrderTimeAmount += dt;
                var progress = (this.creatingOrderTimeAmount/this.creatingOrderTime);
                this.progressBar.getComponent(cc.ProgressBar).progress = progress;
                if (progress >= 1) {
                    this.progressBarState = PB_DISABLED;
                    this.creatingOrderTimeAmount = 0;
                    this.orderWasCreated();
                }
            }
            else if ((this.state == WAITING_FOR_ORDER) && (this.timeToOrderEnd !== null)) {
                this.creatingOrderTimeAmount += dt;
                var progress = (this.creatingOrderTimeAmount/this.timeToOrderEnd);
                this.progressBar.getComponent(cc.ProgressBar).progress = progress;
                if (progress >= 1) {
                    this.progressBarState = PB_DISABLED;
                    this.creatingOrderTimeAmount = 0;
                    this.goOutFromRestaurant();
                }
            }
        }
    },
});
