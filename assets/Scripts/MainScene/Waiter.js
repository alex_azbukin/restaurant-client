const EventsMap = require('EventsMap');
const IS_ACTIVE = 1;
const IS_DISABLED = 2;
const IS_ON_THE_KITCHEN = 3;
const TAKING_ORDER_FROM_KITCHEN = 4;

cc.Class({
    extends: cc.Component,

    properties: {
        meetAGuestButton : { default: null, type: cc.Button},
        takeAnOrderButton : { default: null, type: cc.Button},
        orderAcceptedButton : { default: null, type: cc.Button},
        onTrayButton : { default: null, type: cc.Button},
        bringOrderToKitchenButton : { default: null, type: cc.Button},
        takeOrderFromKitchenButton : { default: null, type: cc.Button},
        trayButton: {default: null, type: cc.Button},
        amountOrderTrayLabel: {default: null, type: cc.Label},

        //settings
        state : { default: IS_ACTIVE},
        takeOrderTableNumber: { default: null},
        currentPosition: {default: 0},
        orders : {default: {}},
        currentOrderNumber: { default: null},
        takingOrder: { default: null},
        onTrayOrders: {default: []},
        maxOrderCount: {default: 3}
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function() {
        cc.log('Waiter loaded');
        cc.systemEvent.on(EventsMap.getEventName('GUEST_ARE_WAITING_FOR_TABLE'), this.onGuestAreWaitingForTable, this);
        cc.systemEvent.on(EventsMap.getEventName('TABLE_WAS_CHECKED'), this.onTableWasChecked, this);
        cc.systemEvent.on(EventsMap.getEventName('GUEST_ARE_READY_FOR_ORDER'), this.onGuestAreReadyForOrder, this);
        cc.systemEvent.on(EventsMap.getEventName('CHEF_TOOK_ORDER'), this.onReturnToHall, this);
        cc.systemEvent.on(EventsMap.getEventName('CALL_WAITER_FOR_TAKE_ORDER'), this.onWaiterCallForOrder, this);
        cc.systemEvent.on(EventsMap.getEventName('FREEZE_BACKGROUND'), this.disableAllEvents, this);
        cc.systemEvent.on(EventsMap.getEventName('UNFREEZE_BACKGROUND'), this.enableAllEvents, this);

        //button events
        this.meetAGuestButton.node.on('click', this.onMeetAGuestButtonClick, this);
        this.takeAnOrderButton.node.on('click', this.onTakeAnOrderButtonClick, this);
        this.bringOrderToKitchenButton.node.on('click', this.onBringOrderToKitchenButtonClick, this);
    },

    start: function() {},

    disableAllEvents: function(event) {
        this.meetAGuestButton.node.off('click', this.onMeetAGuestButtonClick, this);
        this.takeAnOrderButton.node.off('click', this.onTakeAnOrderButtonClick, this);
        this.bringOrderToKitchenButton.node.off('click', this.onBringOrderToKitchenButtonClick, this);
    },

    enableAllEvents: function(event) {
        if ((this.state == IS_ACTIVE) && (this.getAmountOrderOnTray() > 0)) {
            this.trayButton.node.on('click', this.showWaiterTray, this);
        }
        if (this.takeOrderFromKitchenButton.node.opacity == 255) {
            this.takeOrderFromKitchenButton.node.on('click', this.takeOrderFromKitchen, this);
        }
    },

    setState: function(state) {
        this.state = state;
        if ((this.state == IS_ACTIVE) && (this.getAmountOrderOnTray() > 0)) {
            this.showTray();
        }
        else {
            this.trayButton.node.opacity = 0;
            this.trayButton.node.off('click', this.showWaiterTray, this);
        }
    },

    showTray: function() {
        var orderAmount = this.getAmountOrderOnTray();
        this.amountOrderTrayLabel.string = orderAmount + '/' + this.maxOrderCount;
        this.trayButton.node.zIndex = 999;
        this.trayButton.node.on('click', this.showWaiterTray, this);
        this.trayButton.node.getComponent(cc.Animation).play('showTray');
    },

    onGuestAreWaitingForTable: function (event) {
        if (this.state == IS_ACTIVE) {
            this.meetAGuestButton.node.zIndex = 999;
            this.meetAGuestButton.node.getComponent(cc.Animation).play('showButton');
        }
    },

    onMeetAGuestButtonClick: function (event) {
        cc.log('MEET guest.');
        this.meetAGuestButton.node.opacity = 0;
        this.meetAGuestButton.node.zIndex = 0;
        this.meetAGuestButton.node.off('click', this.onMeetAGuestButtonClick, this);
        this.setState(IS_DISABLED);
        var meetAGuest = this.node.getComponent(cc.Animation).play('meetAGuest1');
        meetAGuest.on("finished", this.meetAGuestFinished, this);
        var eventName = EventsMap.getEventName('MEET_A_GUEST_STARTED');
        var event = new Event(eventName);
        cc.systemEvent.dispatchEvent(event);
    },

    meetAGuestFinished: function () {
        var eventName = EventsMap.getEventName('MEET_A_GUEST_FINISHED');
        var event = new Event(eventName);
        event.data = {"position" : 1};
        cc.systemEvent.dispatchEvent(event);
    },

    onTableWasChecked: function (event) {
        cc.log("Waiter : Table was checked : table#" + event.data.tableNumber);
        this.currentPosition = event.data.tableNumber;
        var walkToTableWithGuest = this.node.getComponent(cc.Animation).play('walkToTable' + event.data.tableNumber + 'FromDoor');
        walkToTableWithGuest.on("finished", this.onGuestAreCreatingOrder, this);
    },

    onGuestAreCreatingOrder: function (event) {
        this.setState(IS_ACTIVE);
    },

    onGuestAreReadyForOrder: function (event) {
        if (this.state == IS_ACTIVE) {
            this.takeAnOrderButton.node.zIndex = 999;
            this.takeAnOrderButton.node.getComponent(cc.Animation).play('showButton');
            this.takeOrderTableNumber = event.data.tableNumber;
            this.currentOrderNumber =  event.data.order.id;
            this.orders[event.data.order.id] = event.data.order;
        }
    },

    onTakeAnOrderButtonClick: function () {
        this.setState(IS_DISABLED);
        this.takeAnOrderButton.node.opacity = 0;
        this.takeAnOrderButton.node.zIndex = 0;
        this.takeAnOrderButton.node.off('click', this.onTakeAnOrderButtonClick, this);
        this.currentPosition = this.takeOrderTableNumber;
        cc.log('GO to table # ' + this.takeOrderTableNumber);
        var takeOrderAnimation = this.getComponent(cc.Animation).play('walkFrom' + this.currentPosition + 'PositionToTable' + this.takeOrderTableNumber);
        takeOrderAnimation.on("finished", this.onTakeOrderFinished, this);
        var eventName = EventsMap.getEventName('WAITER_GO_TO_GUEST_FOR_ORDER');
        var event = new Event(eventName);
        event.data = {'tableNumber' : this.takeOrderTableNumber};
        cc.systemEvent.dispatchEvent(event);
    },

    onTakeOrderFinished: function () {
        cc.log('Waiter : Order was accepted.');
        this.setState(IS_ACTIVE);
        this.node.zIndex = 1;
        var eventName = EventsMap.getEventName('ORDER_WAS_ACCEPTED');
        var event = new Event(eventName);
        event.data = {'order' : this.orders[this.currentOrderNumber]};
        cc.systemEvent.dispatchEvent(event);
    },

    _hasOrder: function(orderId) {
        return ( typeof(this.orders[orderId]) == "undefined") ? false : true;
    },

    attachOrder: function (orderId) {
        if ((this.state == IS_ACTIVE) && (this._hasOrder(orderId))) {
            this.bringOrderToKitchenButton.node.zIndex = 999;
            this.bringOrderToKitchenButton.node.getComponent(cc.Animation).play('showButton');
            cc.log(this._hasOrder(orderId));
        }
        else {
            cc.log('OrderID # ' + orderId + ' was deleted.');
            delete this.orders[orderId];
        }
    },

    onBringOrderToKitchenButtonClick: function () {
        cc.log('Bring Order to kitchen.');
        this.setState(IS_DISABLED);
        this.bringOrderToKitchenButton.node.opacity = 0;
        this.bringOrderToKitchenButton.node.zIndex = 0;
        this.bringOrderToKitchenButton.node.off('click', this.onBringOrderToKitchenButtonClick, this);
        var bringOrderToKitchen = this.getComponent(cc.Animation).play('bringFrom' + this.currentPosition + 'Position');
        bringOrderToKitchen.on("finished", this.onOrderWasBroughtOnKitchen, this);
    },

    onOrderWasBroughtOnKitchen: function () {
        this.setState(IS_ON_THE_KITCHEN);
        var eventName = EventsMap.getEventName('ORDER_WAS_BROUGHT_ON_KITCHEN');
        var event = new Event(eventName);
        event.data = {'orders' : this.orders};
        cc.systemEvent.dispatchEvent(event);
        this.orders = {}; //clear list of orders for current waiter
    },

    onReturnToHall: function (event) {
        cc.log('Waiter : return to hall');
        var returnToHall = this.getComponent(cc.Animation).play('fromKitchenToPosition0');
        returnToHall.on("finished", this.onComebackToHallFromKitchen, this);
    },

    onComebackToHallFromKitchen: function () {
        cc.log('Waiter : comeback in hall');
        this.currentPosition = 0;
        this.setState(IS_ACTIVE);
    },

    onWaiterCallForOrder: function (event) {
        cc.log('Waiter : current order ID => ' + event.data.order.id);
        if (this.state == IS_ACTIVE) {
            this.takingOrder = event.data.order;
            this.takeOrderFromKitchenButton.node.zIndex = 999;
            this.takeOrderFromKitchenButton.node.getComponent(cc.Animation).play('showButton');
            this.takeOrderFromKitchenButton.node.on('click', this.takeOrderFromKitchen, this);
        }
    },

    takeOrderFromKitchen: function () {
        cc.log('Go to Kitchen for Order.');
        this.setState(TAKING_ORDER_FROM_KITCHEN);
        this.takeOrderFromKitchenButton.node.zIndex = 0;
        this.takeOrderFromKitchenButton.node.opacity = 0;
        this.takeOrderFromKitchenButton.node.off('click', this.takeOrderFromKitchen, this);
        var eventName = EventsMap.getEventName('WAITER_ARE_TAKING_ORDER_FROM_KITCHEN');
        var event = new Event(eventName);
        cc.systemEvent.dispatchEvent(event);
        var toKitchenAnimation = this.getComponent(cc.Animation).play('fromPosition' + this.currentPosition + 'ToKitchen');
        toKitchenAnimation.on("finished", this.onTakeOrderFromKitchenFinished, this);
    },

    clearCurrentTakingOrder: function () {
        if (this.state != TAKING_ORDER_FROM_KITCHEN) {
            this.takingOrder = null;
        }
    },

    onTakeOrderFromKitchenFinished: function () {
        this.currentPosition = 99;
        var eventName = EventsMap.getEventName('WAITER_TOOK_ORDER');
        var event = new Event(eventName);
        event.data = {"order" : this.takingOrder};
        cc.systemEvent.dispatchEvent(event);
        this.onTrayOrders.push(this.takingOrder);
        this.takingOrder = null;
        cc.log(this.onTrayOrders);
        this.setState(IS_ACTIVE);
    },
    
    getAmountOrderOnTray: function () {
        return this.onTrayOrders.length;
    },

    showWaiterTray: function () {
        var eventName = EventsMap.getEventName('SHOW_DIALOG');
        var event = new Event(eventName);
        event.data = {"dialog" : "WaiterTray", "orders" : this.onTrayOrders};
        cc.systemEvent.dispatchEvent(event);
    }

    // update (dt) {},
});
