const EventsMap = require('EventsMap');
var Guest = require('Guest');
var Waiter = require('Waiter');
var Table = require('Table');

cc.Class({
    extends: cc.Component,

    properties: {
        waiterPrefab : { default: null, type: cc.Prefab},
        guestPrefab : { default : null, type: cc.Prefab},
        tablePrefab: { default: null, type: cc.Prefab},
        guests: [],
        waiters: [],
        tables: [],
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function() {
        this.initWaiters();
        this.initTables();
        this.initGuests();
        cc.log('Main Scene loaded');

        cc.systemEvent.on(EventsMap.getEventName('TABLE_WAS_CHECKED'), this.onTableWasChecked, this);
        cc.systemEvent.on(EventsMap.getEventName('ORDER_WAS_ACCEPTED'), this.onOrderWasAccepted, this);
        cc.systemEvent.on(EventsMap.getEventName('WAITER_ARE_TAKING_ORDER_FROM_KITCHEN'), this.onClearWaiterCurrentOrder, this);
    },

    start: function() {
        this.scheduleOnce(function() {
            this.guests[0].walkToRestaurant(1);
        }, 1);
    },

    initWaiters: function () {
        var waiter = cc.instantiate(this.waiterPrefab);
        waiter.setPosition(cc.p(-200, -240));
        this.node.addChild(waiter);
        this.waiters.push(waiter.getComponent(Waiter));
    },

    initTables: function () {
        var tablesNumber = [0,3];
        tablesNumber.forEach(function (number) {
            var table  = cc.instantiate(this.tablePrefab);
            switch (number) {
                case 0:
                    table.setPosition(cc.p(-100, -290));
                    break;
                case 3:
                    table.setPosition(cc.p(140, -60));
                    break;
            }
            this.node.addChild(table);
            var tableComponent = table.getComponent(Table);
            this.tables.push(tableComponent);
            tableComponent.setNumber(number);
        }.bind(this));
    },
    
    initGuests: function () {
        var guest = cc.instantiate(this.guestPrefab);
        guest.setPosition(cc.p(550, -240));
        this.node.addChild(guest);
        var guestComponent = guest.getComponent(Guest);
        guestComponent.setNumber(1);
        this.guests.push(guestComponent);
    },

    onTableWasChecked: function (event) {
        this.tables.forEach(function (table, index) {
            table.disappearFreeButton();
        });
    },

    onOrderWasAccepted: function (event) {
        this.waiters.forEach(function (waiter, index) {
            waiter.attachOrder(event.data.order.id);
        });
    },

    onClearWaiterCurrentOrder: function (event) {
        this.waiters.forEach(function (waiter, index) {
            waiter.clearCurrentTakingOrder();
        });
    },

    // update (dt) {},
});
