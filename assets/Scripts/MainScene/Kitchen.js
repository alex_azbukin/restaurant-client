const EventsMap = require('EventsMap');
const PB_DISABLED = 1;
const PB_CREATING = 2;

var Order = require('Order');
cc.Class({
    extends: cc.Component,

    properties: {
        orders : {default : {}},
        cookingOrders : {default: {}},
        currentOrder: { default: null},
        acceptOrderButton : { default: null, type: cc.Button},
        order : { default: null, type: cc.Prefab},
        cookingLayout : { default: null, type: cc.Layout}
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function() {
        cc.log('Kitchen was loaded.');
        cc.systemEvent.on(EventsMap.getEventName('ORDER_WAS_BROUGHT_ON_KITCHEN'), this.onOrderInKitchen, this);
        cc.systemEvent.on(EventsMap.getEventName('WAITER_TOOK_ORDER'), this.onRemoveOrder, this);
        cc.systemEvent.on(EventsMap.getEventName('FREEZE_BACKGROUND'), this.disableAllEvents, this);
        //button events
        this.acceptOrderButton.node.on('click', this.onAcceptOrderButtonClick, this);
    },

    start: function() {

    },

    disableAllEvents: function(event) {
        this.acceptOrderButton.node.off('click', this.onAcceptOrderButtonClick, this);
    },

    onOrderInKitchen: function(event) {
        cc.log('Kitchen : order[s] on kitchen');
        cc.log(event.data.orders);
        for(var i in event.data.orders){
            this.orders[i] = event.data.orders[i];
        }
        this.acceptOrderButton.node.opacity = 255;
        this.acceptOrderButton.node.zIndex = 999;
    },

    onAcceptOrderButtonClick: function () {
        cc.log('Kitchen : accept order');
        this.acceptOrderButton.node.opacity = 0;
        this.acceptOrderButton.node.zIndex = 0;
        this.acceptOrderButton.node.off('click', this.onAcceptOrderButtonClick, this);
        var eventName = EventsMap.getEventName('CHEF_TOOK_ORDER');
        var event = new Event(eventName);
        cc.systemEvent.dispatchEvent(event);
        this.startCreatingOrders();
    },
    
    startCreatingOrders: function () {
        cc.log('Chef : start creating orders.');
        Object.getOwnPropertyNames(this.orders).forEach(function (key) {
            console.log('KEY : ' + key);
            console.log('VALUE');
            console.log(this.orders[key]);
            var items = this.orders[key]['items'];
            this.cookingLayout.node.opacity = 255;
            this.cookingOrders[key] = {};
            items.forEach(function (item) {
                var itemNode = cc.instantiate(this.order);
                var itemComponent = itemNode.getComponent(Order);
                itemComponent.init(item, key);
                this.cookingLayout.node.addChild(itemNode);
                this.cookingOrders[key][item.name] = itemNode;
            }.bind(this));
        }.bind(this));
    },

    onRemoveOrder: function(event) {
        var order = event.data.order;
        this.orders[order.id].items.forEach(function(item, index){
            if (item.name == order.name) {
                this.cookingOrders[order.id][item.name].destroy();
                delete this.cookingOrders[order.id][item.name];
                delete this.orders[order.id].items[index];
            }
        }.bind(this));
        if (Object.getOwnPropertyNames(this.orders).length == 0) {
            this.cookingLayout.node.opacity = 0;
        }
    },
    // update (dt) {},
});
