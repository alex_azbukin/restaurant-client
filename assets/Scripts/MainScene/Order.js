const EventsMap = require('EventsMap');
const PB_DISABLED = 1;
const PB_ENABLED = 2;
const ORDER_PENDING = 1;
const ORDER_CREATED = 2;
cc.Class({
    extends: cc.Component,

    properties: {
        picture : { default: null, type: cc.Sprite},
        pictureType1 : {default: null, type: cc.SpriteFrame},
        pictureType2 : {default: null, type: cc.SpriteFrame},
        progressBar : {default: null, type: cc.ProgressBar},
        costAmount : {default: null, type: cc.Label},
        countAmount : {default: null, type: cc.Label},
        statusAmount : {default: null, type: cc.Label},
        timeAmount : {default: null, type: cc.Label},
        startTimeValue : {default: 0},
        timeForCreating : {default: null},
        progressBarState : {default: PB_DISABLED},
        state : {default: ORDER_PENDING},
        takeButton: {default: null, type: cc.Button},
        currentID : {default: null},
        currentName : {default: null},
        currentTable : {default: null},
        details : {default: null}
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function() {
        cc.log('Order was loaded.');
        cc.systemEvent.on(EventsMap.getEventName('FREEZE_BACKGROUND'), this.disableAllEvents, this);
        cc.systemEvent.on(EventsMap.getEventName('UNFREEZE_BACKGROUND'), this.enableAllEvents, this);
        this.takeButton.node.on('click', this.callWaiter, this);
    },

    start: function() {

    },

    disableAllEvents: function(event) {
        this.takeButton.interactable = false;
        this.takeButton.node.off('click', this.callWaiter, this);
    },

    enableAllEvents: function(event) {
        this.takeButton.interactable = true;
        this.takeButton.node.on('click', this.callWaiter, this);
    },

    init: function (order, orderId) {
        cc.log(order);
        this.details = order;
        this.picture.spriteFrame = this['pictureType' + order.type];
        this.costAmount.string = order.cost;
        this.countAmount.string = order.count;
        this.statusAmount.string = order.status;
        this.timeAmount.string = order.time;
        this.timeForCreating = order.time * order.count;
        this.currentID = orderId;
        this.currentName = order.name;
        this.currentTable = order.tableNumber;
        this.progressBarState = PB_ENABLED;
        this.progressBar.node.opacity = 255;
    },

    orderCreated: function() {
        this.progressBar.node.opacity = 0;
        this.takeButton.node.opacity = 255;
        this.progressBarState == PB_DISABLED;
        this.state = ORDER_CREATED;
        this.timeForCreating = null;
    },

    callWaiter: function() {
        var eventName = EventsMap.getEventName('CALL_WAITER_FOR_TAKE_ORDER');
        var event = new Event(eventName);
        event.data = {"order" : {"id" : this.currentID, "name" : this.currentName, "details" : this.details}};
        cc.systemEvent.dispatchEvent(event);
    },

    onDestroy: function() {
        cc.log('Order was destroyed => ' + this.currentID + '|' + this.currentName);
    },

    update: function(dt) {
        if ( (this.progressBarState == PB_ENABLED) && (this.timeForCreating !== null)) {
            this.startTimeValue += dt;
            var progress = (this.startTimeValue/this.timeForCreating);
            this.progressBar.getComponent(cc.ProgressBar).progress = progress;
            if (progress >= 1) {
                this.orderCreated();
            }
        }
    },
});
