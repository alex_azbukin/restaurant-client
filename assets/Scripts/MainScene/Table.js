const EventsMap = require('EventsMap');
const IS_ACTIVE = 1;
const IS_DISABLED = 2;
cc.Class({
    extends: cc.Component,

    properties: {
        number : {default: null},
        state: {default: IS_ACTIVE},
        freeButton: {default: null, type: cc.Button},
        cleanUpButton: {default: null, type: cc.Button},
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function() {
        cc.log('Table Loaded.');
        cc.systemEvent.on(EventsMap.getEventName('MEET_A_GUEST_FINISHED'), this.meetAGuestFinished, this);
        //button events
        this.freeButton.node.on('click', this.onSetTheTable, this);
    },

    start: function() {},

    setNumber: function (number) {
        this.number = number;
    },

    meetAGuestFinished: function () {
        if (this.state == IS_ACTIVE) {
            this.freeButton.node.opacity = 255;
            this.freeButton.node.zIndex = 999;
        }
    },

    onSetTheTable: function () {
        cc.log('TABLE was setted. Number : ' + this.number);
        this.state = IS_DISABLED;
        var eventName = EventsMap.getEventName('TABLE_WAS_CHECKED');
        var event = new Event(eventName);
        event.data = {"tableNumber" : this.number, "state" : this.state};
        cc.systemEvent.dispatchEvent(event);
    },

    disappearFreeButton: function () {
        this.freeButton.node.opacity = 0;
        this.freeButton.node.zIndex = 0;
        this.freeButton.node.off('click', this.onSetTheTable, this);
    }

    // update (dt) {},
});
