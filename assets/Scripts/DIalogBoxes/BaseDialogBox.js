const EventsMap = require('EventsMap');
cc.Class({
    extends: cc.Component,

    properties: {
        background: { default: null, type: cc.Button},
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad: function() {},

    start () {},
    
    open: function () {
        cc.log('OPEN box.');
        this.node.opacity = 255;
        this.node.zIndex = 9999;
        this.freezeBackground();
    },
    
    close: function () {
        cc.log('CLOSE box.');
        this.node.opacity = 0;
        this.node.zIndex = 0;
        this.unfreezeBackground();
    },

    freezeBackground: function () {
        var eventName = EventsMap.getEventName('FREEZE_BACKGROUND');
        var event = new Event(eventName);
        cc.systemEvent.dispatchEvent(event);
    },

    unfreezeBackground: function () {
        var eventName = EventsMap.getEventName('UNFREEZE_BACKGROUND');
        var event = new Event(eventName);
        cc.systemEvent.dispatchEvent(event);
        this.node.destroy();
    }

    // update (dt) {},
});
