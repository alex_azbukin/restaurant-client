const EventsMap = require('EventsMap');
cc.Class({
    extends: cc.Component,

    properties: {
        picture: {default: null, type: cc.Sprite},
        pictureType1: {default: null, type: cc.SpriteFrame},
        pictureType2: {default: null, type: cc.SpriteFrame},
        costLabel: {default: null, type: cc.Label},
        tableNumberLabel: {default: null, type: cc.Label},
        placeOrderButton: {default: null, type: cc.Button},
        isPlacedToggle: {default: null, type: cc.Toggle},
        currentTable: {default: null},
        currentOrder: {default: null}
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function() {
        cc.systemEvent.on(EventsMap.getEventName('CHECKED_ORDER_ON_TRAY'), this.onCheckOrder, this);
        this.placeOrderButton.node.on('click', this.onPlaceOrderClick, this);
        this.isPlacedToggle.node.on('click', this.onChecked, this);
    },

    start () {

    },

    init: function (data) {
        this.picture.spriteFrame = this['pictureType' + data.details.type];
        this.costLabel.string = data.details.cost;
        this.tableNumberLabel.string = data.details.tableNumber;
        this.currentTable = data.details.tableNumber;
        this.currentOrder = data;
    },
    
    onPlaceOrderClick: function () {
        cc.log('PLACE ORDER');
        var eventName = EventsMap.getEventName('PLACE_ORDER');
        var event = new Event(eventName);
        event.data = {"order" : this.currentOrder};
        cc.systemEvent.dispatchEvent(event);
        this.node.destroy();
    },

    onChecked: function() {
        var isChecked = false;
        if (this.isPlacedToggle.node.getComponent(cc.Toggle).isChecked) {
            cc.log('IS checked');
        }
        else {
            cc.log('NOT CHECKED');
            isChecked = true;
        }
        var eventName = EventsMap.getEventName('CHECKED_ORDER_ON_TRAY');
        var event = new Event(eventName);
        event.data = {"isChecked" : isChecked, "tableNumber" : this.currentTable, "order" : this.currentOrder};
        cc.systemEvent.dispatchEvent(event);
    },

    onCheckOrder: function(event) {
        if (this.currentTable != event.data.tableNumber) {
            cc.log(' can NOT check.');
            this.node.opacity = 0;
            this.isPlacedToggle.node.getComponent(cc.Toggle).isChecked = false;
        }
        else {
            if (event.data.order.name != this.currentOrder.name){
                (event.data.isChecked == true) ? this.isPlacedToggle.node.getComponent(cc.Toggle).check() : this.isPlacedToggle.node.getComponent(cc.Toggle).uncheck();
            }
            this.node.opacity = 255;
        }
    },

    onDestroy: function () {
        cc.log('Placed Order Tray destroyed.');
    }

    // update (dt) {},
});
