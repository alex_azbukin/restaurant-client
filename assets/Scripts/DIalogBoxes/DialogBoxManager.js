const EventsMap = require('EventsMap');

cc.Class({
    extends: cc.Component,

    properties: {
        scene: {default: null, type: cc.Node},
        WaiterTrayDialogPrefab: {default: null, type: cc.Prefab},
        WaiterTrayDialog: {default: null, type: cc.Node, visible: false}
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function() {
        cc.systemEvent.on(EventsMap.getEventName('SHOW_DIALOG'), this.onBoxOpen, this);
    },

    start () {},

    onBoxOpen: function (event) {
        var dialogName = event.data.dialog;
        var boxNode = this[dialogName];
        var boxPrefab = this[dialogName + 'DialogPrefab'];
        if (boxPrefab !== null) {
            boxNode = cc.instantiate(boxPrefab);
            boxNode.getComponent(dialogName+'Dialog').show(event.data.orders);
            this.scene.addChild(boxNode);
        }
    }

    // update (dt) {},
});
