const BaseDialogBox = require('BaseDialogBox');
var WaiterTrayItem = require('WaiterTrayItem');
cc.Class({
    extends: BaseDialogBox,

    properties: {
        ordersLayout: {default: null, type: cc.Layout},
        orderPrefab: {default: null, type: cc.Prefab},
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function() {
        cc.log('waiterTrayDialogBox loaded.');
        this.background.node.on('click', this.onClose, this);
    },

    start () {},

    show: function (orders) {
        this.init(orders);
        this.open();
    },

    init: function (data) {
        cc.log('WAITER TRAY Dialog box initialized.');
        for (var i in data) {
            var order = cc.instantiate(this.orderPrefab);
            this.ordersLayout.node.addChild(order);
            order.getComponent(WaiterTrayItem).init(data[i]);
            cc.log(data[i]);
        }
    },
    
    onClose: function () {
        this.close();
    },
    
    onDestroy: function () {
        cc.log('waiterTrayDialogBox destroyed.');
    }

    // update (dt) {},
});
